let p5GCode = function(opt) {
  opt = opt||{};
  this.gcode = [];
  this.paperWidth = opt.paperWidth || 370 ;
  this.paperHeight = opt.paperHeight || 530;

  if(opt.customPenUp) this.penUp = opt.customPenUp;
  else this.penUp = "G0 Z" + (opt.penUp || 5.0);
  if(opt.customPenDown) this.penDown = opt.customPenDown;
  else this.penDown = "G0 Z" + (opt.penDown || 0.0);
  if(opt.customPenWait) this.penWait = opt.customPenWait;
  else this.penWait = "G4 P" + (opt.penWait || 0.1);
  this.motorFeedSlow = opt.motorFeedSlow || 3250.0;
  this.motorFeedFast = opt.motorFeedFast || 3500.0;

  this.biArcs = opt.biArcs || true;
  this.biArcsTolerance = opt.biArcsTolerance || 20;

  this.curvePrecision = opt.curvePrecision || 20;

  this.fileName = opt.fileName || "untitled.ngc";

this.startCode = opt.startCode || [`( Made with p5js / Paper size: ${this.paperWidth}  x ${this.paperHeight} mm )`,
"G21",
this.penUp,
`G0 F${this.motorFeedFast} X0.0 Y0.0`];

  if(!Array.isArray(this.startCode)) this.startCode = [this.startCode];

  this.endCode = opt.endCode || [this.penUp,`G0 X0 Y0 F${this.motorFeedFast}`,"M5","M30"];

  if(!Array.isArray(this.endCode)) this.endCode = [this.endCode];

  this.ratio =1;

  this.currentPixX = 0;
  this.currentPixY = 0;
  this.penIsDown = false;

  this.originUp = opt.originUp || false; // most of plotter have origin on bottom

  this.hasEnd = false;
  this.bindedFunctions= [];
  this.unbinded = false;

  this.firstInit = true;
  this.p5This = null;

  this.init();
  this.new();
};

p5GCode.prototype = {
  init:function(){        
      // change normal functions
      let that = this;
      if(this.firstInit) {
        let originalCreateCanvas = createCanvas;
        createCanvas = function(w,h,renderer){

            if(renderer == WEBGL) {
                console.error("Pas d'export 3d pour l'instant");
            }
            originalCreateCanvas.apply(this, arguments);
            that.setRatio();

        }
        this.firstInit = null;
      }

      this.bind();

      this.setRatio();

  },

  bindRenderer: function(fnName, fn) {
      let that = this;

      let originalFn = p5.Renderer2D.prototype[fnName];
      p5.Renderer2D.prototype[fnName] = function() {
          that.p5This = this;
          fn.apply(that,arguments);
          return originalFn.apply(this, arguments);
      }

      this.bindedFunctions.push({name:fnName,fn:originalFn});
  },

  bind: function(){
    this.bindRenderer("line",this.line);
    this.bindRenderer("point",this.point);
    this.bindRenderer("quad",this.quad);
    this.bindRenderer("rect",this.rect);
    this.bindRenderer("triangle",this.triangle);
    this.bindRenderer("ellipse",this.ellipse);
    this.bindRenderer("arc",this.arc);
    this.bindRenderer("endShape",this.endShape);

    this.unbinded = false;
  },

  unbind: function() {
      while(this.bindedFunctions.length) {
          var toUnbind = this.bindedFunctions.pop();
          p5.Renderer2D.prototype[toUnbind.name] = toUnbind.fn;
      }

      this.unbinded = true;
  },

  new: function() {
      if (this.unbinded) this.init();
      this.gcode = this.startCode.concat();
          this.hasEnd = false;
          this.currentPixX = 0;
          this.currentPixY = 0;
      },

  end:function() {
      if (this.hasEnd) return;
      // G0 Z90.0
      // G0 X0 Y0 => go home
      // M5 => stop spindle
      // M30 => stop eGxecution
      this.gcode = this.gcode.concat( this.endCode);

      this.hasEnd = true;

      this.unbind();

      return this.gcode;
  },

  save: function(name) {
      this.end();
      if(name) this.fileName = name;
      saveStrings(this.gcode,this.fileName,"ngc");
  },

  flush: function() {
    let gcode = this.gcode;
    this.gcode = [];
    return gcode.join('\n');
  },

  setRatio: function() {
      if(width && height) {
          this.ratio = min(this.paperWidth/width, this.paperHeight/height)
      }
      console.log("setRatio",this.ratio,this.paperWidth, width, this.paperHeight, height);
  },

  fromPixelToPaperValue: function(x,y) {
      // normalize the pixel X,Y coordinates to the real paper sheet dimensions expressed in mm
      let tr = drawingContext.getTransform();

      //if(!this.originUp) tr = tr.flipY().translate(0, -height);

      let newPoint = tr.transformPoint(new DOMPointReadOnly(x,y));
      if(!this.originUp) newPoint.y = height-newPoint.y;
      return {x:parseFloat(newPoint.x*this.ratio).toFixed(4),y:parseFloat(newPoint.y*this.ratio).toFixed(4)}; 
  },

  goUp: function(){
      if (!this.penIsDown) return;

      if(this.gcode[this.gcode.length-1] == this.penDown || this.gcode[this.gcode.length-1] == this.penWait) this.gcode.pop();
      else this.gcode.push(this.penUp);

      this.penIsDown = false;
  },

  goDown: function() {
      if(this.penIsDown) return;

      if(this.gcode[this.gcode.length-1] == this.penUp) this.gcode.pop();
      else {
        this.gcode.push(this.penDown);
        if(this.penWait) this.gcode.push(this.penWait);
      }
    
    
      this.penIsDown = true;
  },

  moveTo: function(x,y) {
      let coor = this.fromPixelToPaperValue(x,y);
      this.gcode.push((this.penIsDown? "G1" : "G0")+" X" + coor.x + " Y" + coor.y+" F"+(this.penIsDown? this.motorFeedSlow : this.motorFeedFast ) );

      this.currentPixX = x;
      this.currentPixY = y;
  },

  line: function(x1, y1, x2, y2) {
      if (x1!=this.currentPixX || y1 != this.currentPixY) {
          this.goUp();
          this.moveTo(x1,y1);
      }

      this.goDown();

      this.moveTo(x2,y2);
  },

  point: function(x,y) {
      this.goUp();
      this.moveTo(x,y);
      this.goDown();
      this.goUp();
  },

  quad: function(x1, y1, x2, y2, x3, y3, x4, y4) {
      this.goUp();
      this.moveTo(x1,y1);
      this.goDown();
      this.moveTo(x2,y2);
      this.moveTo(x3,y3);
      this.moveTo(x4,y4);
      this.moveTo(x1,y1);
  },

  rect: function(args) {

      var x = args[0],y = args[1],w = args[2],h = args[3],tl = args[4],tr = args[5],br = args[6],bl = args[7];

      if (typeof tl === 'undefined') {
            // No rounded corners
            this.quad(x,y,x+w,y,x+w,y+h,x,y+h);
          } else {
            // At least one rounded corner
            // Set defaults when not specified
            if (typeof tr === 'undefined') {
              tr = tl;
            }
            if (typeof br === 'undefined') {
              br = tr;
            }
            if (typeof bl === 'undefined') {
              bl = br;
            }

            // corner rounding must always be positive
            var absW = Math.abs(w);
            var absH = Math.abs(h);
            var hw = absW / 2;
            var hh = absH / 2;

            // Clip radii
            if (absW < 2 * tl) {
              tl = hw;
            }
            if (absH < 2 * tl) {
              tl = hh;
            }
            if (absW < 2 * tr) {
              tr = hw;
            }
            if (absH < 2 * tr) {
              tr = hh;
            }
            if (absW < 2 * br) {
              br = hw;
            }
            if (absH < 2 * br) {
              br = hh;
            }
            if (absW < 2 * bl) {
              bl = hw;
            }
            if (absH < 2 * bl) {
              bl = hh;
            }

            // Draw shape
            // ctx.beginPath();
            // ctx.moveTo(x + tl, y);
            // ctx.arcTo(x + w, y, x + w, y + h, tr);
            // ctx.arcTo(x + w, y + h, x, y + h, br);
            // ctx.arcTo(x, y + h, x, y, bl);
            // ctx.arcTo(x, y, x + w, y, tl);
            // ctx.closePath();
          }
  },

  triangle: function(args) {
      var x1 = args[0], y1 = args[1], x2 = args[2], y2 = args[3], x3 = args[4], y3 = args[5];
      this.goUp();
      this.moveTo(x1,y1);
      this.goDown();
      this.moveTo(x2,y2);
      this.moveTo(x3,y3);
      this.moveTo(x1,y1);
  },

  ellipse: function(args) {
    const x = parseFloat(args[0]),
      y = parseFloat(args[1]),
      w = parseFloat(args[2]),
      h = parseFloat(args[3]);

    const kappa = 0.5522847498,
      // control point offset horizontal
      ox = w / 2 * kappa,
      // control point offset vertical
      oy = h / 2 * kappa,
      // x-end
      xe = x + w,
      // y-end
      ye = y + h,
      // x-middle
      xm = x + w / 2,
      ym = y + h / 2; // y-middle

    this.bezierToPoints(x,ym,x, ym - oy, xm - ox, y, xm, y, true);
    //ctx.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
    this.bezierToPoints(xm, y, xm + ox, y, xe, ym - oy, xe, ym, true);
    //ctx.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
    this.bezierToPoints(xe, ym, xe, ym + oy, xm + ox, ye, xm, ye, true);
    //ctx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
    this.bezierToPoints(xm, ye,xm - ox, ye, x, ym + oy, x, ym, true);
    //ctx.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
  },

  arc: function(x, y, w, h, start, stop, mode) {
    const rx = w / 2.0;
    const ry = h / 2.0;
    const epsilon = 0.00001; // Smallest visible angle on displays up to 4K.
    let arcToDraw = 0;
    const curves = [];

    x += rx;
    y += ry;

    // Create curves
    while (stop - start >= epsilon) {
      arcToDraw = Math.min(stop - start, HALF_PI);
      curves.push(p5.Renderer2D.prototype._acuteArcToBezier(start, arcToDraw));
      start += arcToDraw;
    }

    curves.forEach((curve, index) => {
      // if (index === 0) {
      //   this.goUp();
      //   this.moveTo(x + curve.ax * rx, y + curve.ay * ry);
      //   this.goDown();
      // }
      // prettier-ignore
      this.bezierToPoints(x + curve.ax * rx,
        y + curve.ay * ry,
        x + curve.bx * rx, y + curve.by * ry,
        x + curve.cx * rx, y + curve.cy * ry,
        x + curve.dx * rx, y + curve.dy * ry,
        true);
      //ctx.bezierCurveTo(x + curve.bx * rx, y + curve.by * ry,
                          // x + curve.cx * rx, y + curve.cy * ry,
                          // x + curve.dx * rx, y + curve.dy * ry);
    }, this);
    if (mode === PIE) {
      // ctx.lineTo(x, y);
      // ctx.closePath();
      this.moveTo(x,y);
      this.moveTo(x + curves[0].ax * rx, y + curves[0].ay * ry);
    } else if (mode === CHORD) {
      // ctx.closePath();
      this.moveTo(x + curves[0].ax * rx, y + curves[0].ay * ry);
    }
    // ctx.stroke();

  },

  endShape: function(
    mode,
    vertices,
    isCurve,
    isBezier,
    isQuadratic,
    isContour,
    shapeKind
  ) {
    if (vertices.length === 0) {
      return;
    }

    console.log("endShapeGCODE")

    vertices = vertices.concat();
    
    const closeShape = mode === CLOSE;
    let v;
    if (closeShape && !isContour) {
      vertices.push(vertices[0]);
    }
    let i, j;
    const numVerts = vertices.length;
    this.goUp();
    this.moveTo(vertices[0][0],vertices[0][1]);

    if (isCurve && (/*shapeKind === constants.POLYGON || */ shapeKind === null)) { //  POLYGON constant doesn't exist
      if (numVerts > 3) {
        console.log("curve");
        const b = [],
          s = 1 - this.p5This._curveTightness;
          
        //this.drawingContext.beginPath();
        //this.drawingContext.moveTo(vertices[1][0], vertices[1][1]);
        

        for (i = 1; i + 2 < numVerts; i++) {
          v = vertices[i];
          b[0] = [v[0], v[1]];
          b[1] = [
            v[0] + (s * vertices[i + 1][0] - s * vertices[i - 1][0]) / 6,
            v[1] + (s * vertices[i + 1][1] - s * vertices[i - 1][1]) / 6
          ];
          b[2] = [
            vertices[i + 1][0] +
              (s * vertices[i][0] - s * vertices[i + 2][0]) / 6,
            vertices[i + 1][1] + (s * vertices[i][1] - s * vertices[i + 2][1]) / 6
          ];
          b[3] = [vertices[i + 1][0], vertices[i + 1][1]];
          // this.drawingContext.bezierCurveTo(
          //   b[1][0],
          //   b[1][1],
          //   b[2][0],
          //   b[2][1],
          //   b[3][0],
          //   b[3][1]
          // );
          this.bezierToPoints(
            b[0][0],
            b[0][1],
              b[1][0],
              b[1][1],
              b[2][0],
              b[2][1],
              b[3][0],
              b[3][1],
              true
            );
            
        }
        if (closeShape) {
          // this.drawingContext.lineTo(vertices[i + 1][0], vertices[i + 1][1]);
          this.moveTo(vertices[i + 1][0], vertices[i + 1][1]);
        }
        // this._doFillStrokeClose(closeShape);
      }
    } else if (
      isBezier &&
      (/*shapeKind === constants.POLYGON ||*/ shapeKind === null) //  POLYGON constant doesn't exist
    ) {
      console.log("bezier");
      // this.drawingContext.beginPath();
      for (i = 0; i < numVerts; i++) {
        if (vertices[i].isVert) {
          if (vertices[i].moveTo) {
            // this.drawingContext.moveTo(vertices[i][0], vertices[i][1]);
            this.goUp();
            this.moveTo(vertices[i][0], vertices[i][1]);
          } else {
            // this.drawingContext.lineTo(vertices[i][0], vertices[i][1]);
            this.goDown();
            this.moveTo(vertices[i][0], vertices[i][1]);
          }
        } else {
          // this.drawingContext.bezierCurveTo(
          //   vertices[i][0],
          //   vertices[i][1],
          //   vertices[i][2],
          //   vertices[i][3],
          //   vertices[i][4],
          //   vertices[i][5]
          // );
          this.bezierToPoints(
              this.currentPixX,
              this.currentPixY,
              vertices[i][0],
              vertices[i][1],
              vertices[i][2],
              vertices[i][3],
              vertices[i][4],
              vertices[i][5],
              true
            );
        }
      }

      // this._doFillStrokeClose(closeShape);
    } else if (
      isQuadratic &&
      (/*shapeKind === constants.POLYGON ||*/ shapeKind === null) //  POLYGON constant doesn't exist
    ) {
      console.log("quadratic");

      for (i = 0; i < numVerts; i++) {
        if (vertices[i].isVert) {
          if (vertices[i].moveTo) {
            this.goUp();
            this.moveTo(vertices[i][0], vertices[i][1]);
          } else {
            this.goDown();
            this.moveTo(vertices[i][0], vertices[i][1]);
          }
        } else {
            this.quadraticToPoints(
              this.currentPixX,
              this.currentPixY,
              vertices[i][0],
              vertices[i][1],
              vertices[i][2],
              vertices[i][3],
              true
            );
          }
        }



      // this.drawingContext.beginPath();
      // for (i = 0; i < numVerts; i++) {
      //   if (vertices[i].isVert) {
      //     if (vertices[i].moveTo) {
      //       this.drawingContext.moveTo(vertices[i][0], vertices[i][1]);
      //     } else {
      //       this.drawingContext.lineTo(vertices[i][0], vertices[i][1]);
      //     }
      //   } else {
      //     this.drawingContext.quadraticCurveTo(
      //       vertices[i][0],
      //       vertices[i][1],
      //       vertices[i][2],
      //       vertices[i][3]
      //     );
      //   }
      // }
      // this._doFillStrokeClose(closeShape);
    } else {
      if (shapeKind === POINTS) {
        console.log("points TODO");
        // for (i = 0; i < numVerts; i++) {
        //   v = vertices[i];
        //   if (this.p5This._doStroke) {
        //     this._pInst.stroke(v[6]);
        //   }
        //   this._pInst.point(v[0], v[1]);
        // }
      } else if (shapeKind === LINES) {
        console.log("lines");
        for (i = 0; i + 1 < numVerts; i += 2) {
          v = vertices[i];
          if (this.p5This._doStroke) {
            //this._pInst.stroke(vertices[i + 1][6]);
            this.goDown();
          }
          this.moveTo(v[0], v[1], vertices[i + 1][0], vertices[i + 1][1]);
          //this._pInst.line(v[0], v[1], vertices[i + 1][0], vertices[i + 1][1]);
        }
        // for (i = 0; i + 1 < numVerts; i += 2) {
        //   v = vertices[i];
        //   if (this.p5This._doStroke) {
        //     this._pInst.stroke(vertices[i + 1][6]);
        //   }
        //   this._pInst.line(v[0], v[1], vertices[i + 1][0], vertices[i + 1][1]);
        // }
      } else if (shapeKind === TRIANGLES) {
        console.log("triangles TODO");
        // for (i = 0; i + 2 < numVerts; i += 3) {
        //   v = vertices[i];
        //   this.drawingContext.beginPath();
        //   this.drawingContext.moveTo(v[0], v[1]);
        //   this.drawingContext.lineTo(vertices[i + 1][0], vertices[i + 1][1]);
        //   this.drawingContext.lineTo(vertices[i + 2][0], vertices[i + 2][1]);
        //   this.drawingContext.closePath();
        //   if (this.p5This._doFill) {
        //     this._pInst.fill(vertices[i + 2][5]);
        //     this.drawingContext.fill();
        //   }
        //   if (this.p5This._doStroke) {
        //     this._pInst.stroke(vertices[i + 2][6]);
        //     this.drawingContext.stroke();
        //   }
        // }
      } else if (shapeKind === TRIANGLE_STRIP) {
        console.log("triangle_strip TODO");
        // for (i = 0; i + 1 < numVerts; i++) {
        //   v = vertices[i];
        //   this.drawingContext.beginPath();
        //   this.drawingContext.moveTo(vertices[i + 1][0], vertices[i + 1][1]);
        //   this.drawingContext.lineTo(v[0], v[1]);
        //   if (this.p5This._doStroke) {
        //     this._pInst.stroke(vertices[i + 1][6]);
        //   }
        //   if (this.p5This._doFill) {
        //     this._pInst.fill(vertices[i + 1][5]);
        //   }
        //   if (i + 2 < numVerts) {
        //     this.drawingContext.lineTo(vertices[i + 2][0], vertices[i + 2][1]);
        //     if (this.p5This._doStroke) {
        //       this._pInst.stroke(vertices[i + 2][6]);
        //     }
        //     if (this.p5This._doFill) {
        //       this._pInst.fill(vertices[i + 2][5]);
        //     }
        //   }
        //   this._doFillStrokeClose(closeShape);
        // }
      } else if (shapeKind === TRIANGLE_FAN) {
        console.log("triangle_fan TODO");
        // if (numVerts > 2) {
        //   // For performance reasons, try to batch as many of the
        //   // fill and stroke calls as possible.
        //   this.drawingContext.beginPath();
        //   for (i = 2; i < numVerts; i++) {
        //     v = vertices[i];
        //     this.drawingContext.moveTo(vertices[0][0], vertices[0][1]);
        //     this.drawingContext.lineTo(vertices[i - 1][0], vertices[i - 1][1]);
        //     this.drawingContext.lineTo(v[0], v[1]);
        //     this.drawingContext.lineTo(vertices[0][0], vertices[0][1]);
        //     // If the next colour is going to be different, stroke / fill now
        //     if (i < numVerts - 1) {
        //       if (
        //         (this.p5This._doFill && v[5] !== vertices[i + 1][5]) ||
        //         (this.p5This._doStroke && v[6] !== vertices[i + 1][6])
        //       ) {
        //         if (this.p5This._doFill) {
        //           this._pInst.fill(v[5]);
        //           this.drawingContext.fill();
        //           this._pInst.fill(vertices[i + 1][5]);
        //         }
        //         if (this.p5This._doStroke) {
        //           this._pInst.stroke(v[6]);
        //           this.drawingContext.stroke();
        //           this._pInst.stroke(vertices[i + 1][6]);
        //         }
        //         this.drawingContext.closePath();
        //         this.drawingContext.beginPath(); // Begin the next one
        //       }
        //     }
        //   }
        //   this._doFillStrokeClose(closeShape);
        // }
      } else if (shapeKind === QUADS) {
        console.log("quads TODO");
        // for (i = 0; i + 3 < numVerts; i += 4) {
        //   v = vertices[i];
        //   this.drawingContext.beginPath();
        //   this.drawingContext.moveTo(v[0], v[1]);
        //   for (j = 1; j < 4; j++) {
        //     this.drawingContext.lineTo(vertices[i + j][0], vertices[i + j][1]);
        //   }
        //   this.drawingContext.lineTo(v[0], v[1]);
        //   if (this.p5This._doFill) {
        //     this._pInst.fill(vertices[i + 3][5]);
        //   }
        //   if (this.p5This._doStroke) {
        //     this._pInst.stroke(vertices[i + 3][6]);
        //   }
        //   this._doFillStrokeClose(closeShape);
        // }
      } else if (shapeKind === QUAD_STRIP) {
        console.log("quad_strip TODO");
        // if (numVerts > 3) {
        //   for (i = 0; i + 1 < numVerts; i += 2) {
        //     v = vertices[i];
        //     this.drawingContext.beginPath();
        //     if (i + 3 < numVerts) {
        //       this.drawingContext.moveTo(vertices[i + 2][0], vertices[i + 2][1]);
        //       this.drawingContext.lineTo(v[0], v[1]);
        //       this.drawingContext.lineTo(vertices[i + 1][0], vertices[i + 1][1]);
        //       this.drawingContext.lineTo(vertices[i + 3][0], vertices[i + 3][1]);
        //       if (this.p5This._doFill) {
        //         this._pInst.fill(vertices[i + 3][5]);
        //       }
        //       if (this.p5This._doStroke) {
        //         this._pInst.stroke(vertices[i + 3][6]);
        //       }
        //     } else {
        //       this.drawingContext.moveTo(v[0], v[1]);
        //       this.drawingContext.lineTo(vertices[i + 1][0], vertices[i + 1][1]);
        //     }
        //     this._doFillStrokeClose(closeShape);
        //   }
        // }
      } else {
        console.log("other");
        // this.drawingContext.beginPath();
        // this.drawingContext.moveTo(vertices[0][0], vertices[0][1]);
        this.goUp();
        this.moveTo(vertices[0][0], vertices[0][1]);
        for (i = 1; i < numVerts; i++) {
          v = vertices[i];
          if (v.isVert) {
            if (v.moveTo) {
              // this.drawingContext.moveTo(v[0], v[1]);
              this.goUp();
              this.moveTo(v[0], v[1]);
            } else {
              // this.drawingContext.lineTo(v[0], v[1]);
              this.goDown();
              this.moveTo(v[0], v[1]);
            }
          }
        }
        // this._doFillStrokeClose(closeShape);
      }
    }
  },

  bezierToPoints: function(p1x,p1y,c1x,c1y,c2x,c2y,p2x,p2y,toDraw) {
    if(this.biArcs) return this.bezierToBiarcs(p1x,p1y,c1x,c1y,c2x,c2y,p2x,p2y,toDraw);


    let v1,v2,v3,nv1,nv2,t, p1,c1,c2,p2,p;
    let pts = [];

    if(toDraw) {
      this.goUp();
      this.moveTo(p1x,p1y);
      this.goDown();
    }

    for(var i =0;i<=this.curvePrecision;i++) {
      t = i/this.curvePrecision;
      p1 = createVector(p1x,p1y);
      c1 = createVector(c1x,c1y);
      c2 = createVector(c2x,c2y);
      p2 = createVector(p2x,p2y);
      
      v1 = p1.add( p5.Vector.sub(c1,p1).mult(t) );
      v2 = c1.add( p5.Vector.sub(c2,c1).mult(t) );
      v3 = c2.add( p5.Vector.sub(p2,c2).mult(t) );
      
      nv1 = v1.add(p5.Vector.sub(v2,v1).mult(t));
      nv2 = v2.add(p5.Vector.sub(v3,v2).mult(t));
      
      p = nv1.add(nv2.sub(nv1).mult(t));
      
      if(toDraw) {
        this.moveTo(p.x,p.y);
      }

      pts.push(p);
    }

    return pts;
  },

  quadraticToPoints(p1x,p1y,cx,cy,p2x,p2y,toDraw){
    // convert quadratic to cubic
    let c1 = createVector(cx-p1x,cy-p1y).mult(2/3).add(p1x,p1y);
    let c2 = createVector(cx-p2x,cy-p2y).mult(2/3).add(p2x,p2y);
    
    return this.bezierToPoints(p1x,p1y,c1.x, c1.y, c2.x, c2.y, p2x, p2y,toDraw);
      // let v1,v2,p;
      // let pts = [];

      // if(toDraw) {
      //   this.goUp();
      //   this.moveTo(p1x,p1y);
      //   this.goDown();
      // }
      
      // for(let i=0;i<=this.curvePrecision;i++) {
      //   let t = i/this.curvePrecision;
        
      //   p1 = createVector(p1x,p1y);
      //   c1 = createVector(c1x,c1y);
      //   p2 = createVector(p2x,p2y);
        
        
      //   v1 = p1.add( p5.Vector.sub(c1,p1).mult(t) );
      //   v2 = c1.add( p5.Vector.sub(p2,c1).mult(t) );
        
      //   p = v1.add(p5.Vector.sub(v2,v1).mult(t));
        
      //   if(toDraw) {
      //     this.moveTo(p.x,p.y);
      //   }

      //   pts.push(p);
      // }

      // return pts;
  },

  bezierToBiarcs: function(p1x,p1y,c1x,c1y,c2x,c2y,p2x,p2y,toDraw) {

    if(toDraw && p1x!=this.currentPixX || p1y != this.currentPixY) {
      this.goUp();
      this.moveTo(p1x,p1y);
      this.goDown();
    }

    // bezier to bi arc js version of : https://dlacko.org/blog/2016/10/19/approximating-bezier-curves-by-biarcs/
    let nrPointsToCheck=5; 
    let biarcs = [];
    let curves = [];
    
    let bz = {
        p1:createVector(p1x,p1y),
        c1:createVector(c1x,c1y),
        c2:createVector(c2x,c2y),
        p2:createVector(p2x,p2y)
      };
    
    
    if (bz.p1.equals(bz.p2)) {
        let bs = bezierSplit(bz,0.5);
        curves.push(bs[1]);
        curves.push(bs[0]);
    }  else if (bz.p1.equals(bz.c1) || bz.p2.equals(bz.c2))
    {
        curves.push(bz);
    } else {
      let inflex = getInflexionPoints(bz);

      let i1 = IsRealInflexionPoint(inflex[0]);
      let i2 = IsRealInflexionPoint(inflex[1]);

      
      if (i1 && !i2)
      {
          let splited = bezierSplit(bz,inflex[0].re);
          curves.push(splited[1]);
          curves.push(splited[0]);
      }
      else if (!i1 && i2)
      {
          let splited = bezierSplit(bz,inflex[1].re);
          curves.push(splited[1]);
          curves.push(splited[0]);
      }
      else if (i1 && i2)
      {
          let t1 = inflex[0].re;
          let t2 = inflex[1].re;

          // I'm not sure if I need, but it does not hurt to order them
          if (t1 > t2)
          {
              let tmp = t1;
              t1 = t2;
              t2 = tmp;
          }

          // Make the first split and save the first new curve. The second one has to be splitted again
          // at the recalculated t2 (it is on a new curve)

          let splited1 = bezierSplit(bz,t1);

          t2 = (1 - t1) * t2;

          let splited2 = bezierSplit(splited1[1],t2);

          curves.push(splited2[1]);
          curves.push(splited2[0]);
          curves.push(splited1[0]);
      }
      else
      {
          curves.push(bz);
      }
    }
      
      // ---------------------------------------------------------------------------
      // Second, approximate the curves until we run out of them

      
      while (curves.length)
      {
          let bz = curves.pop();

          // ---------------------------------------------------------------------------
          // Calculate the transition point for the BiArc 

          // V: Intersection point of tangent lines
          let C1 = bz.p1.equals(bz.c1) ? bz.c2 : bz.c1;
          let C2 = bz.p2.equals(bz.c2) ? bz.c1 : bz.c2;

          let T1 = {p:bz.p1,m:getSlope(bz.p1,C1)};
          let T2 = {p:bz.p2,m:getSlope(bz.p2,C2)};

          // Edge case: control lines are parallel
          if(T1.m == T2.m)
          {
              let bs = bezierSplit(bz,0.5);
              curves.push(bs[1]);
              curves.push(bs[0]);
              continue;
          }

          let V = lineIntersection(T1,T2);

          // G: incenter point of the triangle (P1, V, P2)
          // http://www.mathopenref.com/coordincenter.html
          let dP2V = bz.p2.dist(V);
          let dP1V = bz.p1.dist(V);
          let dP1P2 = bz.p1.dist(bz.p2);
          let G = (p5.Vector.mult(bz.p1,dP2V).add( p5.Vector.mult(bz.p2,dP1V) ).add(p5.Vector.mult(V,dP1P2) ) ).div(dP2V + dP1V + dP1P2);

          // ---------------------------------------------------------------------------
          // Calculate the BiArc

          let biarc = createBiArc(bz.p1, p5.Vector.sub(bz.p1, C1), bz.p2, p5.Vector.sub(bz.p2, C2), G);

          // ---------------------------------------------------------------------------
          // Calculate the maximum error

          
          let maxDistance = 0;
          let maxDistanceAt = 0;

          let parameterStep = 1 / nrPointsToCheck;


          for (let i = 0; i <= nrPointsToCheck; i++)
          {
              var t = parameterStep * i;
              var u1 = biArcPointAt(biarc,t);
              var u2 = bezierPointAt(bz,t);
              var distance = p5.Vector.sub(u1, u2).mag();

              if (distance > maxDistance)
              {
                  maxDistance = distance;
                  maxDistanceAt = t;
              }
          }

          // Check if the two curves are close enough
          if (maxDistance > this.biArcsTolerance)
          {
              // If not, split the bezier curve the point where the distance is the maximum
              // and try again with the two halfs
              var bs = bezierSplit(bz,maxDistanceAt);
              curves.push(bs[1]);
              curves.push(bs[0]);
          }
          else
          {
              // Otherwise we are done with the current bezier
              biarcs.push(biarc);
          } 
        
        //biarcs.push(biarc);
      }

    if(toDraw) {
      for(let i=0;i<biarcs.length;i++) {
        for(let j=0;j<2;j++){
          let arc = biarcs[i][j];
          //this.fromPixelToPaperValue(x,y);
          // (Vector2 C, float r, float startAngle, float sweepAngle, Vector2 P1, Vector2 P2)
          let coor = this.fromPixelToPaperValue(arc.P2.x,arc.P2.y);
          let coorC = this.fromPixelToPaperValue(arc.C.x-arc.P1.x,arc.C.y-arc.P1.y);
          if(arc.sweepAngle<0) this.gcode.push("G02"+" X" + coor.x + " Y" + coor.y+" I" + coorC.x + " J" + coorC.y+" F"+this.motorFeedSlow );
          else this.gcode.push("G03"+" X" + coor.x + " Y" + coor.y+" I" + coorC.x + " J" + coorC.y+" F"+this.motorFeedSlow );

          this.currentPixX = arc.P2.x;
          this.currentPixY = arc.P2.y;
        }
      }
    }
      
    return biarcs;
    
    
    // BEZIER FUNCTIONS

    function bezierSplit(b,t){
      let p0 = p5.Vector.add( b.p1, p5.Vector.sub(b.c1,b.p1).mult(t) );
      let p1 = p5.Vector.add( b.c1, p5.Vector.sub(b.c2,b.c1).mult(t) );
      let p2 = p5.Vector.add( b.c2, p5.Vector.sub(b.p2,b.c2).mult(t) );

      let p01 = p5.Vector.add( p0, p5.Vector.sub(p1,p0).mult(t) );
      let p12 = p5.Vector.add( p1, p5.Vector.sub(p2,p1).mult(t) );

      let dp = p5.Vector.add( p01, p5.Vector.sub(p12,p01).mult(t) );

      let b1 = {
          p1:b.p1.copy(),
          c1:p0,
          c2:p01,
          p2:dp
        };

      let b2 = {
          p1:dp.copy(),
          c1:p12,
          c2:p2,
          p2:b.p2.copy()
        };

      return [b1,b2];
    }

    function getInflexionPoints(bz){
      let A = p5.Vector.sub(bz.c1,bz.p1);
      let B = p5.Vector.sub(bz.c2, bz.c1).sub(A);
      let C = p5.Vector.sub(bz.p2,bz.c2).sub(A).sub(createVector(2).mult(B) );

      let a = new Complex(B.x * C.y - B.y * C.x, 0);
      let b = new Complex(A.x * C.y - A.y * C.x, 0);
      let c = new Complex(A.x * B.y - A.y * B.x, 0);

      let t1 = b.clone().neg().add( (b.clone().mul(b).sub( Complex(4).mul(a).mul(c) ) ).sqrt() ).div( Complex(2).mul(a) );
      let t2 = b.clone().neg().sub( (b.clone().mul(b).sub( Complex(4).mul(a).mul(c) ).sqrt() ) ).div( Complex(2).mul(a) );

      return [t1, t2];
    }

    function IsRealInflexionPoint(t) {
        return t.im == 0 && t.re > 0 && t.re < 1;
    }

    function bezierPointAt(b, t)
    {
        return p5.Vector.mult(b.p1,Math.pow(1 - t, 3)).add(
                  p5.Vector.mult(b.c1,3 * Math.pow(1 - t, 2) * t) ).add(
                  p5.Vector.mult(b.c2, (3 * (1 - t) * Math.pow(t, 2)) ) ).add (
                  p5.Vector.mult(b.p2, Math.pow(t, 3) ) );
    }

    // LINE FUNCTIONS

    function getSlope(p1, p2)
    {
        if(p2.x == p1.x)
        {
            return NaN;
        }
        else
        {
            return (p2.y - p1.y) / (p2.x - p1.x);
        }
    }

    function lineIntersection(l1,l2)
    {
        if(isNaN(l1.m))
        {
            return lineVerticalIntersection(l1, l2);
        }
        else if(isNaN(l2.m))
        {
            return lineVerticalIntersection(l2, l1);
        }
        else
        {
            var x = (l1.m * l1.p.x - l2.m * l2.p.x - l1.p.y + l2.p.y) / (l1.m - l2.m);
            var y = l1.m * x - l1.m * l1.p.x + l1.p.y;
            return createVector(x, y);
        }
    }


    /// Special case, the first one is vertical (we suppose that the other one is not, 
    /// otherwise they do not cross)
    function lineVerticalIntersection(vl, l)
    {
        var x = vl.p.x;
        var y = l.m * (x - l.p.x) + l.p.y;
        return createVector(x, y);
    }

    function lineCreatePerpendicularAt(P, P1)
    {
        var m = getSlope(P, P1);

        if (m == 0)
        {
            return {p:P, m:NaN};
        }
        else if(isNaN(m))
        {
            return {p:P,m:0};
        }
        else
        {
            return {p:P,m:-1/m};
        }
    }


    // ARCS FUNCTION
    function createBiArc(P1, T1, P2, T2, T) {
      // P1 -> Start point
      // T1 -> Tangent vector at P1
      // P2 -> End point
      // T2 -> Tangent vector at P2
      // T  -> Transition point

      let sum = 0;
      sum += (T.x - P1.x) * (T.y + P1.y);
      sum += (P2.x - T.x) * (P2.y + T.y);
      sum += (P1.x - P2.x) * (P1.y + P2.y);
      var cw = sum < 0;

      // Calculate perpendicular lines to the tangent at P1 and P2
      var tl1 = lineCreatePerpendicularAt(P1, p5.Vector.add(P1, T1) );
      var tl2 = lineCreatePerpendicularAt(P2, p5.Vector.add(P2, T2) );

      // Calculate the perpendicular bisector of P1T and P2T
      var P1T2 = p5.Vector.add(P1, T).div(2); //(P1 + T) / 2;
      var pbP1T = lineCreatePerpendicularAt(P1T2, T);

      var P2T2 = p5.Vector.add(P2, T).div(2);
      var pbP2T = lineCreatePerpendicularAt(P2T2, T);

      // The origo of the circles are at the intersection points
      var C1 = lineIntersection(tl1,pbP1T);
      var C2 = lineIntersection(tl2,pbP2T);

      // Calculate the radii
      var r1 = p5.Vector.sub(C1, P1).mag();
      var r2 = p5.Vector.sub(C2, P2).mag();

      // Calculate start and sweep angles
      var startVector1 = p5.Vector.sub(P1, C1);
      var endVector1 = p5.Vector.sub(T, C1);
      var startAngle1 = Math.atan2(startVector1.y, startVector1.x);
      var sweepAngle1 = Math.atan2(endVector1.y, endVector1.x) - startAngle1;

      var startVector2 = p5.Vector.sub(T, C2);
      var endVector2 = p5.Vector.sub(P2, C2);
      var startAngle2 = Math.atan2(startVector2.y, startVector2.x);
      var sweepAngle2 = Math.atan2(endVector2.y, endVector2.x) - startAngle2;

      // Adjust angles according to the orientation of the curve
      if (cw && sweepAngle1 < 0) sweepAngle1 = 2 * Math.PI + sweepAngle1;
      if (!cw && sweepAngle1 > 0) sweepAngle1 = sweepAngle1 - 2 * Math.PI;
      if (cw && sweepAngle2 < 0) sweepAngle2 = 2 * Math.PI + sweepAngle2;
      if (!cw && sweepAngle2 > 0) sweepAngle2 = sweepAngle2 - 2 * Math.PI;

      // (Vector2 C, float r, float startAngle, float sweepAngle, Vector2 P1, Vector2 P2)
      A1 = {C:C1, r:r1, startAngle:startAngle1, sweepAngle:sweepAngle1, P1:P1, P2:T};
      A2 = {C:C2, r:r2, startAngle:startAngle2, sweepAngle:sweepAngle2, P1:T, P2:P2};

      return [A1,A2];
    }


    function biArcPointAt(arcs,t)
    {
      let A1 = arcs[0];
      let A2 = arcs[1];

        var s = ( A1.r * Math.abs(A1.sweepAngle) ) / ( (A1.r * Math.abs(A1.sweepAngle) ) + (A2.r * Math.abs(A2.sweepAngle) ) );

        if (t <= s)
        {
            return arcPointAt(A1, t/s);
        }
        else
        {
            return arcPointAt(A2, (t - s) / (1 - s) );
        }
    }

    function arcPointAt(arc, t)
    {
        var x = arc.C.x + arc.r * Math.cos(arc.startAngle + t * arc.sweepAngle);
        var y = arc.C.y + arc.r * Math.sin(arc.startAngle + t * arc.sweepAngle);
        return createVector(x, y);
    }
  },

  pointsToGcode: function (pts){

      for(var i=1;i<pts.length;i++) {
        this.moveTo(pts[i].x,pts[i].y);
      }
  },

  //direct control
  up:function(){
    this.goUp();
  },
  down:function(){
    this.goDown();
  },
  move:function(x,y, relative) {
    if(relative) this.gcode.push("G91");
    this.gcode.push((this.penIsDown? "G1" : "G0")+" X" + x + " Y" + y +" F"+(this.penIsDown? this.motorFeedSlow : this.motorFeedFast ) );
    if(relative) this.gcode.push("G90");
  }
};


/*
Complex.js v2.1.1 12/05/2020
https://github.com/infusion/Complex.js/

Copyright (c) 2022, Robert Eisele (robert@xarg.org)
Dual licensed under the MIT or GPL Version 2 licenses.
*/
(function(q){function n(){throw SyntaxError("Invalid Param");}function p(a,b){var c=Math.abs(a),e=Math.abs(b);if(0===a)return Math.log(e);if(0===b)return Math.log(c);if(3E3>c&&3E3>e)return.5*Math.log(a*a+b*b);a/=2;b/=2;return.5*Math.log(a*a+b*b)+Math.LN2}function d(a,b){if(!(this instanceof d))return new d(a,b);var c={re:0,im:0};if(void 0===a||null===a)c.re=c.im=0;else if(void 0!==b)c.re=a,c.im=b;else switch(typeof a){case "object":"im"in a&&"re"in a?(c.re=a.re,c.im=a.im):"abs"in a&&"arg"in a?!Number.isFinite(a.abs)&&
Number.isFinite(a.arg)?c=d.INFINITY:(c.re=a.abs*Math.cos(a.arg),c.im=a.abs*Math.sin(a.arg)):"r"in a&&"phi"in a?!Number.isFinite(a.r)&&Number.isFinite(a.phi)?c=d.INFINITY:(c.re=a.r*Math.cos(a.phi),c.im=a.r*Math.sin(a.phi)):2===a.length?(c.re=a[0],c.im=a[1]):n();break;case "string":c.im=c.re=0;var e=a.match(/\d+\.?\d*e[+-]?\d+|\d+\.?\d*|\.\d+|./g),f=1,h=0;null===e&&n();for(var l=0;l<e.length;l++){var m=e[l];" "!==m&&"\t"!==m&&"\n"!==m&&("+"===m?f++:"-"===m?h++:("i"===m||"I"===m?(0===f+h&&n()," "===
e[l+1]||isNaN(e[l+1])?c.im+=parseFloat((h%2?"-":"")+"1"):(c.im+=parseFloat((h%2?"-":"")+e[l+1]),l++)):((0===f+h||isNaN(m))&&n(),"i"===e[l+1]||"I"===e[l+1]?(c.im+=parseFloat((h%2?"-":"")+m),l++):c.re+=parseFloat((h%2?"-":"")+m)),f=h=0))}0<f+h&&n();break;case "number":c.im=0;c.re=a;break;default:n()}this.re=c.re;this.im=c.im}var g=Math.cosh||function(a){return 1E-9>Math.abs(a)?1-a:.5*(Math.exp(a)+Math.exp(-a))},k=Math.sinh||function(a){return 1E-9>Math.abs(a)?a:.5*(Math.exp(a)-Math.exp(-a))};d.prototype=
{re:0,im:0,sign:function(){var a=this.abs();return new d(this.re/a,this.im/a)},add:function(a,b){var c=new d(a,b);return this.isInfinite()&&c.isInfinite()?d.NAN:this.isInfinite()||c.isInfinite()?d.INFINITY:new d(this.re+c.re,this.im+c.im)},sub:function(a,b){var c=new d(a,b);return this.isInfinite()&&c.isInfinite()?d.NAN:this.isInfinite()||c.isInfinite()?d.INFINITY:new d(this.re-c.re,this.im-c.im)},mul:function(a,b){var c=new d(a,b);return this.isInfinite()&&c.isZero()||this.isZero()&&c.isInfinite()?
d.NAN:this.isInfinite()||c.isInfinite()?d.INFINITY:0===c.im&&0===this.im?new d(this.re*c.re,0):new d(this.re*c.re-this.im*c.im,this.re*c.im+this.im*c.re)},div:function(a,b){var c=new d(a,b);if(this.isZero()&&c.isZero()||this.isInfinite()&&c.isInfinite())return d.NAN;if(this.isInfinite()||c.isZero())return d.INFINITY;if(this.isZero()||c.isInfinite())return d.ZERO;a=this.re;b=this.im;var e=c.re,f=c.im;if(0===f)return new d(a/e,b/e);if(Math.abs(e)<Math.abs(f))return c=e/f,e=e*c+f,new d((a*c+b)/e,(b*
c-a)/e);c=f/e;e=f*c+e;return new d((a+b*c)/e,(b-a*c)/e)},pow:function(a,b){var c=new d(a,b);a=this.re;b=this.im;if(c.isZero())return d.ONE;if(0===c.im){if(0===b&&0<a)return new d(Math.pow(a,c.re),0);if(0===a)switch((c.re%4+4)%4){case 0:return new d(Math.pow(b,c.re),0);case 1:return new d(0,Math.pow(b,c.re));case 2:return new d(-Math.pow(b,c.re),0);case 3:return new d(0,-Math.pow(b,c.re))}}if(0===a&&0===b&&0<c.re&&0<=c.im)return d.ZERO;var e=Math.atan2(b,a),f=p(a,b);a=Math.exp(c.re*f-c.im*e);b=c.im*
f+c.re*e;return new d(a*Math.cos(b),a*Math.sin(b))},sqrt:function(){var a=this.re,b=this.im,c=this.abs();if(0<=a){if(0===b)return new d(Math.sqrt(a),0);var e=.5*Math.sqrt(2*(c+a))}else e=Math.abs(b)/Math.sqrt(2*(c-a));a=0>=a?.5*Math.sqrt(2*(c-a)):Math.abs(b)/Math.sqrt(2*(c+a));return new d(e,0>b?-a:a)},exp:function(){var a=Math.exp(this.re);return new d(a*Math.cos(this.im),a*Math.sin(this.im))},expm1:function(){var a=this.re,b=this.im,c=Math.expm1(a)*Math.cos(b);var e=Math.PI/4;-e>b||b>e?e=Math.cos(b)-
1:(e=b*b,e*=e*(e*(e*(e*(e*(e*(e/20922789888E3-1/87178291200)+1/479001600)-1/3628800)+1/40320)-1/720)+1/24)-.5);return new d(c+e,Math.exp(a)*Math.sin(b))},log:function(){var a=this.re,b=this.im;return new d(p(a,b),Math.atan2(b,a))},abs:function(){var a=this.re;var b=this.im,c=Math.abs(a),e=Math.abs(b);3E3>c&&3E3>e?a=Math.sqrt(c*c+e*e):(c<e?(c=e,e=a/b):e=b/a,a=c*Math.sqrt(1+e*e));return a},arg:function(){return Math.atan2(this.im,this.re)},sin:function(){var a=this.re,b=this.im;return new d(Math.sin(a)*
g(b),Math.cos(a)*k(b))},cos:function(){var a=this.re,b=this.im;return new d(Math.cos(a)*g(b),-Math.sin(a)*k(b))},tan:function(){var a=2*this.re,b=2*this.im,c=Math.cos(a)+g(b);return new d(Math.sin(a)/c,k(b)/c)},cot:function(){var a=2*this.re,b=2*this.im,c=Math.cos(a)-g(b);return new d(-Math.sin(a)/c,k(b)/c)},sec:function(){var a=this.re,b=this.im,c=.5*g(2*b)+.5*Math.cos(2*a);return new d(Math.cos(a)*g(b)/c,Math.sin(a)*k(b)/c)},csc:function(){var a=this.re,b=this.im,c=.5*g(2*b)-.5*Math.cos(2*a);return new d(Math.sin(a)*
g(b)/c,-Math.cos(a)*k(b)/c)},asin:function(){var a=this.re,b=this.im,c=(new d(b*b-a*a+1,-2*a*b)).sqrt();a=(new d(c.re-b,c.im+a)).log();return new d(a.im,-a.re)},acos:function(){var a=this.re,b=this.im,c=(new d(b*b-a*a+1,-2*a*b)).sqrt();a=(new d(c.re-b,c.im+a)).log();return new d(Math.PI/2-a.im,a.re)},atan:function(){var a=this.re,b=this.im;if(0===a){if(1===b)return new d(0,Infinity);if(-1===b)return new d(0,-Infinity)}var c=a*a+(1-b)*(1-b);a=(new d((1-b*b-a*a)/c,-2*a/c)).log();return new d(-.5*a.im,
.5*a.re)},acot:function(){var a=this.re,b=this.im;if(0===b)return new d(Math.atan2(1,a),0);var c=a*a+b*b;return 0!==c?(new d(a/c,-b/c)).atan():(new d(0!==a?a/0:0,0!==b?-b/0:0)).atan()},asec:function(){var a=this.re,b=this.im;if(0===a&&0===b)return new d(0,Infinity);var c=a*a+b*b;return 0!==c?(new d(a/c,-b/c)).acos():(new d(0!==a?a/0:0,0!==b?-b/0:0)).acos()},acsc:function(){var a=this.re,b=this.im;if(0===a&&0===b)return new d(Math.PI/2,Infinity);var c=a*a+b*b;return 0!==c?(new d(a/c,-b/c)).asin():
(new d(0!==a?a/0:0,0!==b?-b/0:0)).asin()},sinh:function(){var a=this.re,b=this.im;return new d(k(a)*Math.cos(b),g(a)*Math.sin(b))},cosh:function(){var a=this.re,b=this.im;return new d(g(a)*Math.cos(b),k(a)*Math.sin(b))},tanh:function(){var a=2*this.re,b=2*this.im,c=g(a)+Math.cos(b);return new d(k(a)/c,Math.sin(b)/c)},coth:function(){var a=2*this.re,b=2*this.im,c=g(a)-Math.cos(b);return new d(k(a)/c,-Math.sin(b)/c)},csch:function(){var a=this.re,b=this.im,c=Math.cos(2*b)-g(2*a);return new d(-2*k(a)*
Math.cos(b)/c,2*g(a)*Math.sin(b)/c)},sech:function(){var a=this.re,b=this.im,c=Math.cos(2*b)+g(2*a);return new d(2*g(a)*Math.cos(b)/c,-2*k(a)*Math.sin(b)/c)},asinh:function(){var a=this.im;this.im=-this.re;this.re=a;var b=this.asin();this.re=-this.im;this.im=a;a=b.re;b.re=-b.im;b.im=a;return b},acosh:function(){var a=this.acos();if(0>=a.im){var b=a.re;a.re=-a.im;a.im=b}else b=a.im,a.im=-a.re,a.re=b;return a},atanh:function(){var a=this.re,b=this.im,c=1<a&&0===b,e=1-a,f=1+a,h=e*e+b*b;a=0!==h?new d((f*
e-b*b)/h,(b*e+f*b)/h):new d(-1!==a?a/0:0,0!==b?b/0:0);b=a.re;a.re=p(a.re,a.im)/2;a.im=Math.atan2(a.im,b)/2;c&&(a.im=-a.im);return a},acoth:function(){var a=this.re,b=this.im;if(0===a&&0===b)return new d(0,Math.PI/2);var c=a*a+b*b;return 0!==c?(new d(a/c,-b/c)).atanh():(new d(0!==a?a/0:0,0!==b?-b/0:0)).atanh()},acsch:function(){var a=this.re,b=this.im;if(0===b)return new d(0!==a?Math.log(a+Math.sqrt(a*a+1)):Infinity,0);var c=a*a+b*b;return 0!==c?(new d(a/c,-b/c)).asinh():(new d(0!==a?a/0:0,0!==b?-b/
0:0)).asinh()},asech:function(){var a=this.re,b=this.im;if(this.isZero())return d.INFINITY;var c=a*a+b*b;return 0!==c?(new d(a/c,-b/c)).acosh():(new d(0!==a?a/0:0,0!==b?-b/0:0)).acosh()},inverse:function(){if(this.isZero())return d.INFINITY;if(this.isInfinite())return d.ZERO;var a=this.re,b=this.im,c=a*a+b*b;return new d(a/c,-b/c)},conjugate:function(){return new d(this.re,-this.im)},neg:function(){return new d(-this.re,-this.im)},ceil:function(a){a=Math.pow(10,a||0);return new d(Math.ceil(this.re*
a)/a,Math.ceil(this.im*a)/a)},floor:function(a){a=Math.pow(10,a||0);return new d(Math.floor(this.re*a)/a,Math.floor(this.im*a)/a)},round:function(a){a=Math.pow(10,a||0);return new d(Math.round(this.re*a)/a,Math.round(this.im*a)/a)},equals:function(a,b){var c=new d(a,b);return Math.abs(c.re-this.re)<=d.EPSILON&&Math.abs(c.im-this.im)<=d.EPSILON},clone:function(){return new d(this.re,this.im)},toString:function(){var a=this.re,b=this.im,c="";if(this.isNaN())return"NaN";if(this.isInfinite())return"Infinity";
Math.abs(a)<d.EPSILON&&(a=0);Math.abs(b)<d.EPSILON&&(b=0);if(0===b)return c+a;0!==a?(c=c+a+" ",0>b?(b=-b,c+="-"):c+="+",c+=" "):0>b&&(b=-b,c+="-");1!==b&&(c+=b);return c+"i"},toVector:function(){return[this.re,this.im]},valueOf:function(){return 0===this.im?this.re:null},isNaN:function(){return isNaN(this.re)||isNaN(this.im)},isZero:function(){return 0===this.im&&0===this.re},isFinite:function(){return isFinite(this.re)&&isFinite(this.im)},isInfinite:function(){return!(this.isNaN()||this.isFinite())}};
d.ZERO=new d(0,0);d.ONE=new d(1,0);d.I=new d(0,1);d.PI=new d(Math.PI,0);d.E=new d(Math.E,0);d.INFINITY=new d(Infinity,Infinity);d.NAN=new d(NaN,NaN);d.EPSILON=1E-15;"function"===typeof define&&define.amd?define([],function(){return d}):"object"===typeof exports?(Object.defineProperty(d,"__esModule",{value:!0}),d["default"]=d,d.Complex=d,module.exports=d):q.Complex=d})(this);