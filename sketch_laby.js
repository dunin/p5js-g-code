function setup() {
  createCanvas(370*2, 530*2);
  var cellWidth = 10;
  var margin = 40;

  var cellMid = cellWidth/2;
  var cellsWidth = Math.floor((width-margin)/cellWidth);
  var cellsHeight = Math.floor((height-margin)/cellWidth);

  // offsets for drawing
  var offsetX = (width-(cellsWidth*cellWidth+1) )/2 -0.5;
  var offsetY = (height-(cellsHeight*cellWidth+1) )/2 -0.5;
  var colorWall = color(45);
  var colorSolution = color(255, 104, 0);

  

  // background(155);
  var cells, lastCell, randValue;

  var btnGen = createButton("generate");
  btnGen.position(width+10, 20);
  btnGen.mousePressed(generate);
  var sliderRand = createSlider(0,100,50);
  sliderRand.position(width+10,50);
  sliderRand.changed(generate);


  var checkWall= createCheckbox('walls', true);
  checkWall.changed(drawLabyrinthe);
  checkWall.position(width+10, 80);
  var checkSolution= createCheckbox('solution', false);
  checkSolution.position(width+10, 100);
  checkSolution.changed(drawLabyrinthe);
  var btnExpWalls = createButton("walls g-code");
  btnExpWalls.position(width+10, 140);
  btnExpWalls.mousePressed(function(){
  	var gcode = new p5GCode({customPenUp:"M3 S1000", customPenDown:"M4 S1000"});
  	drawWall();
  	gcode.save("walls.ngc");
  	drawLabyrinthe();
  });

  var btnExpSolution = createButton("solution g-code");
  btnExpSolution.position(width+10, 170);
  btnExpSolution.mousePressed(function(){
  	var gcode = new p5GCode({customPenUp:"M3 S1000", customPenDown:"M4 S1000"});
  	drawSolution();
  	gcode.save("solution.ngc");
  	drawLabyrinthe();
  });

  generate()

  drawLabyrinthe();

  function generate() {
  	randValue = sliderRand.value()/100;
  	cells = [];
	for (var i=0;i<cellsWidth; i++) {
		cells.push([]);
		for(var j=0;j<cellsHeight;j++){
			cells[i].push ({ prev:null, n:true, s:true, e:true, w:true, visited:false, x:i, y:j, deadEnd: false });
		}
	}

	cells[0][0].w = false; // entrace
	cells[0][0].visited =true;
	cells[cellsWidth-1][cellsHeight-1].e = false; // exit

	lastCell = cells[0][0];

	while(explore());

	drawLabyrinthe();
  }
  

	function explore() {
		var walls = ["n","s","e","w"].sort(()=>Math.random()-randValue);

		for (var i=0;i<walls.length;i++) {
			if (lastCell[walls[i]]) {
				var newCell = getCell(lastCell,walls[i]);
				if(newCell && !newCell.visited) {
					carve(lastCell,walls[i]);
					carve(newCell,walls[i],true);
					newCell.prev = lastCell;
					newCell.visited = true;
					lastCell = newCell;
					return true;
				}
			}
		}

		lastCell.deadEnd = true;
		if(lastCell.x ===0 && lastCell.y===0) return false;

		while(lastCell.deadEnd) lastCell = lastCell.prev;
		return true;
	}

	function getWall(cell,wall){
		return cells[cell.x][cell.y][wall];
	}

	function getCell(cell, dir){
		if(dir==="n" && cell.y!=0) return cells[cell.x][cell.y-1];
		if(dir==="s" && cell.y!=cellsHeight-1) return cells[cell.x][cell.y+1];
		if(dir==="w" && cell.x!=0) return cells[cell.x-1][cell.y];
		if(dir==="e" && cell.x!=cellsWidth-1) return cells[cell.x+1][cell.y];
		return false;
	}

	function carve(cell,wall,opposite) {
		if(opposite) wall = ({"n":"s","s":"n","w":"e","e":"w"})[wall];
		cell[wall] = false;
	}

	function drawLabyrinthe() {
		// dessin du labyrinthe
		background(255);
		if(checkWall.checked())  drawWall();

	  if(checkSolution.checked()) drawSolution();
	  //gcode.end();
	}

	function drawWall() {
		stroke(colorWall);

	  for (var i=0;i<cells.length; i++) {
	  	for(var j=0;j<cells[i].length;j++){
	  		if(cells[i][j].s) line(offsetX+i*cellWidth,offsetY+(j+1)*cellWidth,offsetX+(i+1)*cellWidth, offsetY+(j+1)*cellWidth);
	  		if(cells[i][j].e) line(offsetX+(i+1)*cellWidth,offsetY+j*cellWidth,offsetX+(i+1)*cellWidth, offsetY+(j+1)*cellWidth);
	  	}
	  }

	  line(offsetX,offsetY,offsetX+cellsWidth*cellWidth, offsetY);
	  line(offsetX,offsetY+cellWidth,offsetX, offsetY+cellsHeight*cellWidth);
	}

	function drawSolution() {
		// solution
	  lastCell = cells[cellsWidth-1][cellsHeight-1];

	  stroke(colorSolution);
	  while(lastCell.x!=0 || lastCell.y !=0) {
	  	line(offsetX + cellMid + lastCell.x*cellWidth,
	  		offsetY + cellMid + lastCell.y*cellWidth,
	  		offsetX + cellMid + lastCell.prev.x*cellWidth,
	  		offsetY + cellMid + lastCell.prev.y*cellWidth
	  		)
	  	lastCell = lastCell.prev;
	  }

	  line(offsetX - cellMid, offsetY+cellMid, offsetX + cellMid, offsetY+cellMid);
	  line(offsetX + (cellsWidth-0.5)*cellWidth, offsetY + (cellsHeight-0.5)*cellWidth,
	  	offsetX + (cellsWidth+0.5)*cellWidth, offsetY + (cellsHeight-0.5)*cellWidth);

	}

}
