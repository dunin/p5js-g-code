function setup() {
    var gcode = new p5GCode({paperWidth:210, paperHeight:210});

    createCanvas(400, 400);
    noFill();

    beginShape();
    vertex(30, 20);
    bezierVertex(80, 0, 80, 75, 30, 75);
    bezierVertex(45, 45, 78, 36, 58, 62);
    endShape();

    /*translate(width/2,height/2);
    makeGrid();
    rotate(Math.PI / 8);
    makeGrid();*/

    //gcode.save("test_transform.ngc");
}

function makeGrid() {
    let x;
    for(var i=0;i<40;i++) {
        x = i*5 - 100;
        line(x, -100, x, 100 );
    }
}